# tsview

**tsview** is a [Flask](https://flask.palletsprojects.com/) web app that accepts timeseries (sensor) data over HTTP, stores them in an sqlite database and provides a [Dash/Plotly](https://dash.plotly.com/) dashboard visualizing the timeseries.
Configuration is done in YAML, a documented example can be found in `config.yml`. Start with
```
python run.py -c config.yml
```
or in production with a WSGI server using `wsgi.py` as entrypoint.

## run with uwsgi
make sure you have the uwsgi python plugin installed (packaged separately on several distributions).
`uwsgi --plugin python --http 127.0.0.1:8000 --master -p 4 -w tsboard.app:app`

## Dependencies
python libraries beyond the standard library:
- `numpy`
- `dash` (including `flask`, `plotly`)
- `pyyaml`

## Build
to package this project, run `python -m build`.
