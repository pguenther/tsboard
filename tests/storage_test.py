from storage import *
import numpy as np
import proplot as pplt
import matplotlib

import logging
logging.basicConfig(level=logging.DEBUG)
matplotlib.pyplot.set_loglevel (level = 'warning')

bins = LogBinning(start=3, factor=3, count=8)
st = Storage("test.db", default_binning=bins)

N = 1000
a, b = st.get_data_extent("name1")
if b is None:
    b = 0
    v = [0]
else:
    v = [st["name1"].root_table.get_last_row()[1]]

gap = np.random.randint(30000) - 10000
print(gap)
t = list(range(b + gap, b + gap + 10000, 10))
for _ in t[:-1]:
    v.append(v[-1] + np.random.random(1)[0] - 0.5)
st.store_lists({'name1' : (t, v)})

fig, ax = pplt.subplots()
for res in reversed([10, 30, 60, 100, 300, 600, 1000, 2000, 3000]):
    ax.plot(*st.get("name1", resolution=res), linewidth=0.1, label=f"{res}")

ax.legend()
fig.savefig("storage_test.pdf")

#print(st.get('name1'))

