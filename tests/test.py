import numpy as np
import random
import proplot as pplt

def regenerate_layer(layer, timestamps, values):
    bin_width = layer
    binned_timestamps = []
    binned_values = []


    current_bin_center = timestamps[0] #+ bin_width / 2
    is_in_bin = lambda t, c : t > c
    current_bin_values = []

    i = 0
    while i < len(timestamps):
        if timestamps[i] < current_bin_center + bin_width / 2 and timestamps[i] >= current_bin_center - bin_width / 2:
            # is in bin
            current_bin_values.append(values[i])
            i += 1
        elif len(current_bin_values) > 0:
            binned_timestamps.append(current_bin_center)
            binned_values.append(np.average(current_bin_values))
            current_bin_values = []
            current_bin_center += bin_width
        else:
            current_bin_values = []
            current_bin_center = timestamps[i] #+ bin_width / 2

    if len(current_bin_values) > 0:
        binned_timestamps.append(current_bin_center)
        binned_values.append(np.average(current_bin_values))


    return binned_timestamps, binned_values

timestamps = np.concatenate([np.linspace(1, 100, 800), np.linspace(105, 143, 380), np.linspace(188, 230, 400)])
timestamps += np.random.rand(len(timestamps)) * 0.1 - 0.5
values = [0]
for _ in range(len(timestamps) - 1):
    values.append(values[-1] + random.random() - 0.5)

l1 = regenerate_layer(5, timestamps, values)
l2 = regenerate_layer(15, timestamps, values)
l2_b = regenerate_layer(15, *l1)
l3 = regenerate_layer(55, timestamps, values)

fig, ax = pplt.subplots()
ax.plot(timestamps, values)
ax.plot(*l1)
ax.plot(*l2)
ax.plot(*l2_b)
ax.plot(*l3)
fig.savefig("test.pdf")
