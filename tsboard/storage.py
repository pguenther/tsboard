import sqlite3
from collections.abc import Mapping, Reversible
import numpy as np
import logging
import threading

if not sqlite3.threadsafety == 3:
    import sys
    print(f"Sqlite3 library is not compiled with threadsafety level 3, which is required by this application. It is compiled with threadsafety {sqlite3.threadsafety}, sqlite version {sqlite3.version}, python version {sys.version}. Refer to https://docs.python.org/3/library/sqlite3.html#sqlite3.threadsafety for more information. Quitting")
    exit(1)

# https://stackoverflow.com/a/49507642/2581056
# https://sqlite.org/forum/info/fc008fb3c5a0ee97
sqlite3.register_adapter(np.int64, lambda v: int(v))
sqlite3.register_adapter(np.int32, lambda v: int(v))

class LogBinning(Mapping, Reversible):
    def __init__(self, start, factor, count):
        self.start = start
        self.factor = factor
        self.count = count

    def __getitem__(self, key):
        if key >= self.count:
            raise KeyError(f"{key} not in binning.")

        key_int = int(key)

        return self.start * self.factor**key_int

    def __iter__(self):
        #def it():
        for i in range(self.count):
            yield i

        #return it

    def __len__(self):
        return self.count

    def __reversed__(self):
        return reversed(list(iter(self)))

    def __repr__(self):
        return f"LogBinning(start={repr(self.start)}, factor={self.factor}, count={self.count})"

class StorageTable:
    """
    Representation of one SQL table, interfacing with the SQL backend
    """
    def __init__(self, name, storage, cumulate=False):
        self._name = name
        self._storage = storage
        self._cumulate = cumulate

        self._layers = []

        self._create_table()
        self._weight_max_diff = 10

    def _create_table(self):
        cur = self._storage.cursor()
        cur.execute(f"CREATE TABLE IF NOT EXISTS {self._name} (timestamp INTEGER UNIQUE, value REAL, weight REAL);")
        cur.close()
        self._storage.commit()

    def _get_weight(self, this_timestamp, new_timestamps, previous_rows):
        i = len(new_timestamps)
        if i >= 2:
            w = this_timestamp - new_timestamps[-1]
            w_compare = new_timestamps[-1] - new_timestamps[-2]
        elif i == 1 and len(previous_rows) >= 1:
            w = this_timestamp - new_timestamps[-1]
            w_compare = new_timestamps[-1] - previous_rows[0][0]
        elif i == 0 and len(previous_rows) >= 2:
            w = this_timestamp - previous_rows[0][0]
            w_compare = previous_rows[0][0] - previous_rows[1][0]
        elif len(previous_rows) == 1:
            w = this_timestamp - previous_rows[0][0]
            w_compare = 0
        elif len(previous_rows) == 0:
            w = 1
            w_compare = 0
        else:
            raise Error("This should not happen")

        if w > self._weight_max_diff * w_compare:
            w = w_compare 

        return w

    def _sort_data(self, data):
        if len(data) == 3:
            t, v, w = data
            idx = np.argsort(t)
            return np.array(t)[idx], np.array(v)[idx], np.array(w)[idx]
        elif len(data) == 2:
            t, v = data
            idx = np.argsort(t)
            return np.array(t)[idx], np.array(v)[idx]
        else:
            raise ValueError("data must be a tuple of 2 or 3 lists (timestamps, values[, weights])")

    def _add_missing_weights(self, sorted_data, previous_rows):
        if len(sorted_data) == 3:
            return sorted_data

        full_data = ([], sorted_data[1], [])
        for t in sorted_data[0]:
            w = self._get_weight(t, full_data[0], previous_rows)
            full_data[0].append(t)
            full_data[2].append(w)

        return full_data

    def _cumulate_data(self, sorted_data, previous_rows):
        if not self._cumulate:
            return sorted_data

        if len(previous_rows) == 0:
            v = 0.0
        else:
            v = previous_rows[0][1]

        v_cumul = []
        for v_ in sorted_data[1]:
            v += v_
            v_cumul.append(v)

        return sorted_data[0], v_cumul, sorted_data[2]

    def store(self, data, force_full_regen=False):
        prevrows = self.get_last_rows(2)
        sorted_data = self._sort_data(data)
        sorted_data_w = self._add_missing_weights(sorted_data, prevrows)
        sorted_data_wc = self._cumulate_data(sorted_data_w, prevrows)

        # bubble before storing the data so layers can lazily only regenerate 
        # the last bin (and new bins)
        self.bubble(sorted_data_wc, force_full_regen=force_full_regen)

        # if first new row is an update to the last existing row (equal 
        # timestamps), update it first
        if len(sorted_data_wc[0]) > 0 and len(prevrows) > 0 and sorted_data_wc[0][0] == prevrows[0][0]:
            self.update_last_row(sorted_data_wc[1][0], sorted_data_wc[2][0])
            sorted_data_wc = (sorted_data_wc[0][1:], sorted_data_wc[1][1:], sorted_data_wc[2][1:])

        with self._storage.write_lock:
            cur = self._storage.cursor()
            try:
                cur.executemany(f"INSERT INTO {self._name} VALUES (?, ?, ?)", zip(*sorted_data_wc))
            except sqlite3.IntegrityError as e:
                logging.error(f"SQLite error: '{e}'. Maybe you are trying to insert a row with a timestamp that is already in the database? Timestamps have to be unique.")
            finally:
                cur.close()
                self._storage.commit()

    def get(self, min_timestamp=0, max_timestamp=None):
        """
        returns list of timestamps and list of values
        """
        ts = []
        vals = []

        cur = self._storage.cursor()
        if max_timestamp is None:
            cur.execute(f"SELECT timestamp, value FROM {self._name} WHERE timestamp > (?) ORDER BY timestamp ASC", (min_timestamp, ))
        else:
            cur.execute(f"SELECT timestamp, value FROM {self._name} WHERE timestamp > (?) AND timestamp < (?) ORDER BY timestamp ASC", (min_timestamp, max_timestamp))

        while True:
            l = cur.fetchmany()
            if len(l) == 0:
                break

            for ts_, v_ in l:
                ts.append(ts_)
                vals.append(v_)

        cur.close()

        return ts, vals

    def __len__(self):
        return self.get_row_count()

    def get_row_count(self):
        cur = self._storage.cursor()
        cur.execute(f"SELECT COUNT(*) as count FROM {self._name}")
        c = cur.fetchone()[0]
        cur.close()
        return c

    def get_full(self, min_timestamp=0, max_timestamp=None):
        ts = []
        vals = []
        weights = []

        cur = self._storage.cursor()
        if max_timestamp is None:
            cur.execute(f"SELECT timestamp, value, weight FROM {self._name} WHERE timestamp > (?) ORDER BY timestamp ASC", (min_timestamp, ))
        else:
            cur.execute(f"SELECT timestamp, value, weight FROM {self._name} WHERE timestamp > (?) AND timestamp < (?) ORDER BY timestamp ASC", (min_timestamp, max_timestamp))

        while True:
            l = cur.fetchmany()
            if len(l) == 0:
                break

            for ts_, v_, w_ in l:
                ts.append(ts_)
                vals.append(v_)
                weights.append(w_)

        cur.close()

        return ts, vals, weights

    def get_last_row(self):
        try:
            return self.get_last_rows(1)[0]
        except IndexError:
            return None

    def get_last_rows(self, n):
        cur = self._storage.cursor()
        cur.execute(f"SELECT timestamp, value, weight FROM {self._name} ORDER BY timestamp DESC LIMIT {int(n)}")
        res = cur.fetchall()
        cur.close()

        return res

    def update_last_row(self, new_value, new_weight):
        last = self.get_last_row()
        if last is None:
            raise ValueError("Cannot update last entry of empty table")

        with self._storage.write_lock:
            cur = self._storage.cursor()
            cur.execute(f"UPDATE {self._name} SET value = (?), weight = (?) WHERE timestamp = (?);",
                        (new_value, new_weight, last[0]))
            cur.close()
            self._storage.commit()

    def get_minimum_timestamp(self):
        cur = self._storage.cursor()
        cur.execute(f"SELECT timestamp FROM {self._name} ORDER BY timestamp ASC LIMIT 1")
        min_ts = cur.fetchone()
        cur.close()

        if min_ts is None:
            return None
        else:
            return int(min_ts[0])

    def get_maximum_timestamp(self):
        cur = self._storage.cursor()
        cur.execute(f"SELECT timestamp FROM {self._name} ORDER BY timestamp DESC LIMIT 1")
        max_ts = cur.fetchone()
        cur.close()

        if max_ts is None:
            return None
        else:
            return int(max_ts[0])

    def get_data_extent(self):
        return self.get_minimum_timestamp(), self.get_maximum_timestamp()

    def register_layer(self, layer):
        self._layers.append(layer)

    def bubble(self, data, force_full_regen):
        for layer in self._layers:
            layer.store(data, force_full_regen)

    def _truncate(self):
        """
        truncate the table. intended to be used for layers only after a
        complete regeneration happened.
        """
        cur = self._storage.cursor()
        # sqlite version
        cur.execute(f"DELETE FROM {self._name};")
        # mysql version
        #cur.execute(f"TRUNCATE TABLE {self._name};")
        cur.close()


class StorageLayer(StorageTable):
    """
    Extension of StorageTable, representing a layer containing the binned 
    average value of another StorageLayer or StorageTable.
    """
    def __init__(self, name, base, storage, bin_width):
        # layers do not cumulate, only the underlying root table does
        super().__init__(name, storage, cumulate=False)
        self._base = base
        self._bin_width = bin_width

        self._base.register_layer(self)


    def need_full_regeneration(self, new_data):
        layer_last_row = self.get_last_row()

        if layer_last_row is None:
            full_regen = False
        else:
            full_regen = any(ts < layer_last_row[0] - self._bin_width / 2 
                             for ts in new_data[0])

        return full_regen

    def store(self, data, force_full_regen):
        new_data, full_regen = self._regenerate(data, force_full_regen)

        if full_regen:
            self._truncate()

        # regenerate all layers above this layer fully if this did
        # this is the easy method and probably should not cost too much runtime
        # since upper layers are exponentially thinner.
        # since the lower layer is cleared, this should happen even without
        # passing force_full_regen anyways.
        #
        # usually this shouldnt happen anyways
        super().store(new_data, force_full_regen=full_regen)

    def full_regen(self):
        self.store(([], [], []), True)

    def _regenerate(self, new_data, force_full_regen):
        """
        regenerates a layer given by its ID

        if available_layer_data is given, bin only the new data
        so that it can be appended to the existing data.
        In that case, the last existing row of the layer table must be UPDATED
        with the first row this function returns, while the rest should be appended

        available_layer_data is a 3-tuple of the last (in the timestep sense)
        row of the layer table, and new data is a 2-tuple of lists of timestamp and values
        """

        binned_timestamps = []
        binned_values = []
        binned_weights = []

        full_regen = force_full_regen or self.need_full_regeneration(new_data)

        timestamps, values, weights = new_data

        layer_last_row = self.get_last_row()
        if not full_regen and layer_last_row is not None:
            current_bin_center = layer_last_row[0]
            current_bin_values_sum = layer_last_row[1] * layer_last_row[2]
            current_bin_weight = layer_last_row[2]
        else:
            logging.debug(f"full regeneration of table {self._name}")
            old_ts, old_values, old_weights = self._base.get_full()
            newts = list(old_ts) + list(timestamps)
            idx = np.argsort(newts)
            timestamps = np.array(newts)[idx]
            values = np.array(list(old_values) + list(values))[idx]
            weights = np.array(list(old_weights) + list(weights))[idx]

            if len(timestamps) == 0:
                return ([], [], []), True

            current_bin_center = timestamps[0] #+ self._bin_width / 2
            current_bin_values_sum = 0
            current_bin_weight = 0

        i = 0
        while i < len(timestamps):
            #print(i, "ts", timestamps[i], "cbc", current_bin_center, "bw", self._bin_width, "v", values[i])
            if timestamps[i] < current_bin_center + self._bin_width / 2 and timestamps[i] >= current_bin_center - self._bin_width / 2:
                # is in bin
                current_bin_values_sum += values[i] * weights[i]
                current_bin_weight += weights[i]
                i += 1
            elif current_bin_weight > 0:
                binned_timestamps.append(current_bin_center)
                binned_values.append(current_bin_values_sum / current_bin_weight)
                binned_weights.append(current_bin_weight)
                current_bin_values_sum = 0
                current_bin_weight = 0
                current_bin_center += self._bin_width
            else:
                current_bin_values_sum = 0
                current_bin_weight = 0
                current_bin_center = timestamps[i] #+ self._bin_width / 2

        if current_bin_weight > 0:
            binned_timestamps.append(current_bin_center)
            binned_values.append(current_bin_values_sum / current_bin_weight)
            binned_weights.append(current_bin_weight)

        return (binned_timestamps, binned_values, binned_weights), full_regen


class StorageEntity:
    """
    Higher-level class which represents one named timeseries, managing its
    raw data and a set of layers that are kept up-to-date.

    The most efficient layer is automatically selected when retrieving data.

    Each layer is referencing the next-precise layer (or the raw data
    eventually) for highest efficiency per default. Change with the 
    option all_layers_from_raw_data
    """
    def __init__(self, name, storage, bins, all_layers_from_raw_data=False, cumulate=False):
        self._name = name
        self._storage = storage
        self._bins = bins
        self._bin_table_name = f"_{self._name}_bins"
        self._cumulate = cumulate

        self._layers = []

        with self._storage.write_lock:
            self._create_tables(all_layers_from_raw_data)
            self._update_bins()

    def _create_tables(self, all_layers_from_raw_data):
        self._root_table = StorageTable(self._name, self._storage, cumulate=self._cumulate)
        last_layer = self._root_table
        for b in self._bins:
            l = StorageLayer(f"_{self._name}_{b}", last_layer, self._storage, self._bins[b])
            self._layers.append(l)
            if not all_layers_from_raw_data:
                last_layer = l

        with self._storage.write_lock:
            cur = self._storage.cursor()
            cur.execute(f"CREATE TABLE IF NOT EXISTS {self._bin_table_name} (bin_id INTEGER UNIQUE, bin_width INTEGER);")
            cur.close()
            self._storage.commit()

    def _fill_if_empty(self):
        if len(self._layers) == 0:
            return

        # we only need the first empty layer because the rest is bubbled
        for l in self._layers:
            if l.get_row_count() == 0:
                l.full_regen()
                break

    def _is_binning_consistent(self):
        cur = self._storage.cursor()
        cur.execute(f"SELECT bin_id, bin_width FROM {self._bin_table_name} ORDER BY bin_id ASC;")
        stored_bins = {k: v for k, v in cur.fetchall()}
        cur.close()

        for b in self._bins:
            if not b in stored_bins:
                return False
            if not self._bins[b] == stored_bins[b]:
                return False

        return True
            
    def _overwrite_stored_binning(self):
        with self._storage.write_lock:
            cur = self._storage.cursor()
            cur.execute(f"DELETE FROM {self._bin_table_name};")
            cur.executemany(f"INSERT INTO {self._bin_table_name} VALUES (?, ?)", list(self._bins.items()))
            cur.close()
            self._storage.commit()

    def _update_bins(self):
        if self._is_binning_consistent():
            self._fill_if_empty()
        else:
            logging.info("Configured binning does not match the database binning. Regenerating all layers.")
            self._overwrite_stored_binning()
            if not len(self._layers) == 0:
                self._layers[0].full_regen()


    def store(self, data):
        """
        store data and update the layers with new data

        if they are not appended to the end, all layers are regenerated.
        this can likely be solved much more efficient, but it should seldomly/never
        occur anyways.
        """
        self._root_table.store(data)

    def get_data_extent(self):
        return self._root_table.get_data_extent()

    @property
    def root_table(self):
        return self._root_table

    def get(self, min_timestamp=None, max_timestamp=None, resolution=100):
        if min_timestamp is None:
            min_timestamp = self._root_table.get_minimum_timestamp()

        if max_timestamp is None:
            max_timestamp = self._root_table.get_maximum_timestamp()

        if min_timestamp is None or max_timestamp is None:
            # root table empty, no data
            return [], []

        # select layer
        for l in reversed(self._bins):
            #print("ts", max_timestamp, min_timestamp)
            c = (max_timestamp - min_timestamp) / self._bins[l]
            if c > resolution:
                use = self._layers[l]
                break
        else:
            use = self._root_table

        # retrieve data
        return use.get(min_timestamp=min_timestamp, max_timestamp=max_timestamp)

    def get_last_datapoint(self):
        lr = self._root_table.get_last_row()
        if lr is None:
            return None
        else:
            return lr[:2]

class Storage:
    """
    A database class storing timeseries in an SQLite database.

    This class essentially manages the database connection and creates
    StorageEntity objects on demand for each accessed timeseries.

    Timestamps are expected to be in milliseconds.
    """
    def __init__(self, path=None, db_name=None, default_binning=LogBinning(start=1 * 60 * 1e3, factor=3, count=8), cumulate=[], binning=None):
        self._entities = {}
        self._bins = default_binning

        if binning is None:
            self._bins_timeseries = None
        else:
            self._bins_timeseries = binning

        self._cumulate = cumulate

        if path is None:
            logging.warning("No database path given - using in-memory database, all data will be lost on shutdown")
            self._conn = sqlite3.connect(":memory:", check_same_thread=False)
        else:
            self._conn = sqlite3.connect(path, check_same_thread=False)

        self._db_name = db_name
        self.write_lock = threading.RLock()

        if db_name is not None:
            with self.write_lock:
                cur = self._conn.cursor()
                cur.execute(f"CREATE DATABASE IF NOT EXISTS {db_name};")
                cur.close()
                self.commit()

    def __del__(self):
        self._conn.close()

    def cursor(self):
        return self._conn.cursor()

    def commit(self):
        try:
            r = self._conn.commit()
        except sqlite3.OperationalError as e:
            raise
            logging.debug(f"OperationalError during commit: {e}")
        except sqlite3.DatabaseError as e:
            raise
            logging.debug(f"DatabaseError during commit: {e}")

        return r

    def _load_table(self, name):
        if name not in self._entities:
            if self._bins_timeseries is not None and name in self._bins_timeseries:
                bins = self._bins_timeseries[name]
            else:
                bins = self._bins

            self._entities[name] = StorageEntity(name, self, bins, cumulate=name in self._cumulate)

    def __getitem__(self, key):
        self._load_table(key)
        return self._entities[key]

    def store_single_name(self, name, rows):
        self[name].store(rows)

    def store(self, tuples):
        """
        expects list of 3-tuples (name, timestamp, value)
        """
        d = {}
        for n_, t_, v_ in tuples:
            if not n_ in d:
                d[n_] = ([], [])
            d[n_][0].append(t_)
            d[n_][1].append(v_)

        self.store_lists(d)

    def store_lists(self, d):
        """
        expects a dict of 2-tuples of list ([timestamp], [value]) for each name
        """
        for n_, data in d.items():
            self.store_single_name(n_, data)

    def get(self, name, min_timestamp=None, max_timestamp=None, resolution=100):
        return self[name].get(min_timestamp=min_timestamp, max_timestamp=max_timestamp, resolution=resolution)

    def get_data_extent(self, name):
        return self[name].get_data_extent()
