from datetime import datetime, timedelta

import yaml

import numpy as np

from flask import Flask, request, current_app, Blueprint

import plotly.graph_objects as go
from dash import Dash, html, dcc, callback, Output, Input, State

from .storage import Storage
from .config import cfg

def store_route():
    ts_now = int(datetime.now().timestamp() * 1000)
    tuples = []
    failures = 0
    for point in request.json:
        if 'timestamp' in point:
            try:
                ts = int(point['timestamp'])
            except ValueError:
                failures += 1
                continue
        else:
            ts = ts_now

        if 'name' in point:
            name = point['name'] # FIXME escape somehow
        else:
            failures += 1
            continue

        try:
            value = float(point['value'])
        except ValueError:
            failures += 1
            continue

        tuples.append((name, ts, value))

    current_app.config['_storage'].store(tuples)
    
    if failures == 0:
        return "ok"
    else:
        return f"{failures} values could not be parsed. Is the timestamp an integer (millisecond precision) and the value a float?"

def get_data_route():
    if 'start' in request.json:
        min_timestamp = request.json['start']
    else:
        min_timestamp = None

    if 'end' in request.json:
        max_timestamp = request.json['end']
    else:
        max_timestamp = None

    name = request.json['name']

    if 'resolution' in request.json:
        resolution = int(request.json['resolution'])
    else:
        resolution = np.inf

    data = current_app.config['_storage'].get(name, min_timestamp=min_timestamp, max_timestamp=max_timestamp, resolution=resolution)

    return {name : {"t": data[0], "v" : data[1]}}


class PlotApp(Dash):
    def __init__(self, name, server, storage, **kwargs):
        super().__init__(name, server=server, **kwargs)
        self.item_count = 0
        self.page_layouts = {}
        self._storage = storage

        self.layout = html.Div([dcc.Location(id='url', refresh=False),
                                html.Div(id='page-content')])

        @self.callback(Output('page-content', 'children'), Input('url', 'pathname'))
        def display_page(pathname):
            if pathname[-1] == '/':
                pathname = pathname[:-1]

            if pathname in self.page_layouts:
                return self.page_layouts[pathname]
            else:
                return [html.P("Page not found.")]



    def _add_graph_layout(self, items):
        self.graph_layout += items
        self.layout = html.Div(self.graph_layout)

    def _get_timerange(self, timeseries_name, show_last, relayoutData):
        if relayoutData is None or 'xaxis.range[0]' not in relayoutData or 'xaxis.range[1]' not in relayoutData:
            _, end = self._storage.get_data_extent(timeseries_name)
            start = (datetime.fromtimestamp(end / 1000) - show_last).timestamp() * 1000 
        elif type(relayoutData['xaxis.range[0]']) is float or type(relayoutData['xaxis.range[1]']) is float:
            _, end = self._storage.get_data_extent(timeseries_name)
            start = (datetime.fromtimestamp(end / 1000) - show_last).timestamp() * 1000 
        else:
            start = int(datetime.fromisoformat(relayoutData['xaxis.range[0]']).timestamp() * 1000)
            end = int(datetime.fromisoformat(relayoutData['xaxis.range[1]']).timestamp() * 1000)

        timerange = (start, end)

        return timerange

    def create_page(self, page):
        elements = []
        for item_cfg in page['items']:
            elements += self.create_item(item_cfg)

        pathname = page['url']
        if pathname[-1] == '/':
            pathname = pathname[:-1]

        self.page_layouts[pathname] = html.Div(elements)

    def create_plot(self, _item_id, _auto_refresh_input, title=None, xlabel="", yaxis=None, layout={}, style={}, **kwargs):
        xmargin = max(0, (len(yaxis) - 2) * kwargs['yaxis_offset'])

        fig_layout = {
                'xaxis' : {
                    'domain' : [xmargin, 1 - xmargin],
                    'title' : xlabel
                    },
                'title': "" if title is None else f"<b>{title}</b>",
                'margin' : {
                    'l': 0,
                    'r': 0,
                    't': 50,
                    'b': 30
                    },
                } | layout

        for i, yax in enumerate(yaxis):
            # https://plotly.com/python/multiple-axes/#multiple-axes
            offset = xmargin - kwargs['yaxis_offset'] * int(i / 2)
            if i % 2 == 1:
                offset = 1 - offset

            axis_name = f'yaxis{i+1 if i > 0 else ""}'

            fig_layout[axis_name] = {
                    'title': yax['label'],
                    'anchor': 'x' if i < 2 else 'free',
                    'overlaying' : 'y',
                    'side': 'right' if i % 2 == 1 else 'left',
                    'position': offset
                    }

            # delete overlaying property for the first yaxis
            # if kept, only one timeseries is shown (the last one)
            # I don't know why at the moment
            if i == 0:
                del fig_layout[axis_name]['overlaying']

        div_name = f'graph-{_item_id}'
        cb_args = [Output(div_name, 'figure'),
                   Input(div_name, 'relayoutData'),
                   _auto_refresh_input]


        @self.callback(*cb_args)
        def update_graph(relayoutData, n):
            fig = go.Figure()
            fig['layout']['uirevision'] = "0"

            if kwargs['show_values_in_graph']:
                value_str = []

            for y_id, yax in enumerate(yaxis):
                for t_id, ts_name in enumerate(yax['timeseries']):
                    if isinstance(ts_name, dict):
                        label = list(ts_name.values())[0]
                        ts_name = list(ts_name.keys())[0]
                    else:
                        label = ts_name

                    if kwargs['show_values_in_graph']:
                        lr = self._storage[ts_name].get_last_datapoint()
                        if lr is not None:
                            value_str.append(f"{label} {lr[1]:.04g}{yax['unit']}")

                    if kwargs['show_values_in_legend']:
                        lr = self._storage[ts_name].get_last_datapoint()
                        if lr is not None:
                            label = f"{label} {lr[1]:.04g}{yax['unit']}"

                    timerange = self._get_timerange(ts_name, kwargs['show_last'], relayoutData)
                    ts, v = self._storage.get(ts_name,
                                              min_timestamp=timerange[0], 
                                              max_timestamp=timerange[1],
                                              resolution=kwargs['resolution'])
                    ts_dt = [datetime.fromtimestamp(t / 1000) for t in ts]

                    additional_trace_kw = {} if y_id == 0 else {'yaxis': f'y{y_id+1}'}
                    trace_args = {'x': ts_dt, 'y': v, 'name': label} | additional_trace_kw

                    fig.add_trace(go.Scatter(**trace_args))

            if kwargs['show_values_in_graph']:
                fig.add_annotation(
                        showarrow=False,
                        text="<b>" + ", ".join(value_str) + "</b>",
                        font=dict(size=kwargs['show_values_in_graph']['size']),
                        xref='paper',
                        yref='paper',
                        x=kwargs['show_values_in_graph']['x'],
                        y=kwargs['show_values_in_graph']['y'],
                    )
            fig.update_layout(**fig_layout)

            return fig


        return [dcc.Graph(id=div_name, config={'scrollZoom': True}, style=style)]

    def create_string(self, _item_id, _auto_refresh_input, string, style={}, **kwargs):
        div_name = f'string-{_item_id}'
        children = [html.P(string, style=style)] 

        return [html.Div(children, id=div_name)]

    def create_current_values(self, _item_id, _auto_refresh_input, timeseries, style={}, date_format="%H:%M:%S", **kwargs):
        style = {
                'font-size': '20px',
                'font-family': 'sans-serif',
                'font-weight': 'bold',
                'text-align': 'center'
                } | style
        div_name = f'values-{_item_id}'
        @self.callback([Output(div_name, 'children'), _auto_refresh_input])
        def update_cv(n):
            lines = []
            for t in timeseries:
                if isinstance(t, dict):
                    unit = list(t.values())[0]
                    t = list(t.keys())[0]
                else:
                    unit = ""

                lr = self._storage[t].get_last_datapoint()
                if lr is None:
                    string = f"{t}: No data"
                else:
                    date_str = datetime.fromtimestamp(lr[0] / 1000).strftime(date_format)
                    string = f"{t}: {lr[1]:.04g}{unit} (last updated {date_str})"

                lines.append(html.P(string, style=style))
            return [lines]

        return [html.Div(id=div_name)]
                

    def create_item(self, item_cfg):
        auto_refresh_id = f'refresh-{self.item_count}'
        auto_refresh_input = Input(auto_refresh_id, 'n_intervals')

        if item_cfg['type'] == 'string':
            elements = self.create_string(self.item_count, auto_refresh_input, **item_cfg)
        elif item_cfg['type'] == 'current values':
            elements = self.create_current_values(self.item_count, auto_refresh_input, **item_cfg)
        elif item_cfg['type'] == 'plot':
            elements = self.create_plot(self.item_count, auto_refresh_input, **item_cfg)
        else:
            raise ValueError("Type not valid")

        self.item_count += 1

        if 'auto_refresh' in item_cfg:
            interval = dcc.Interval(id=auto_refresh_id, interval=item_cfg['auto_refresh'].seconds * 1000, n_intervals=0)
        else:
            interval = dcc.Interval(id=auto_refresh_id, disabled=True, n_intervals=0)
            

        return elements + [interval]


def create_app(conf_path_cmdline=None):
    conf = cfg(conf_path_cmdline)
    storage = Storage(**conf['storage'])

    app = Flask(__name__)
    app.config.update(**conf)
    app.config.update(_storage = storage)

    bp_store = Blueprint('store', __name__)
    bp_store.route('/store', methods=['POST'])(store_route)
    app.register_blueprint(bp_store, url_prefix=conf['base_url'])

    bp_get = Blueprint('data', __name__)
    bp_get.route('/data', methods=['GET'])(get_data_route)
    app.register_blueprint(bp_get, url_prefix=conf['base_url'])

    plotapp = PlotApp(__name__,
                      server=app,
                      storage=storage,
                      url_base_pathname=conf['base_url'],
                      title=conf['title'],
                      update_title=conf['update_title']
                     )

    for page in conf['pages']:
        plotapp.create_page(page)

    # if seemingly unreasonable "callback not found, did you forget @" errors
    # pop up, replace app -> plotapp for debugging (hopefully better traceback)
    return app


