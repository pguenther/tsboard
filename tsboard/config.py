import os
import yaml
from datetime import timedelta
from .storage import LogBinning

default_cfg_path = '/etc/tsboard.yml'
cfg_defaults = {'plots' : [],
                'storage': {},
                'host' : '127.0.0.1',
                'debug' : True,
                'base_url' : None,
                'requests_prefix': None,
                'routes_prefix': None,
                'title': 'tsboard',
                'update_title': None
                }

cfg_yaxis_defaults = {
        'label' : '',
        'unit' : '',
        'timeseries' : {}
        }

cfg_show_values_in_graph_defaults = {
        'size' : 16,
        'y' : 1,
        'x' : 0.5
        }

cfg_plot_defaults = {
        'show_last': None,
        'yaxis_offset': 0.04,
        'resolution': 100,
        'show_values_in_legend' : False,
        'show_values_in_graph' : {}
        }

def parse_interval(s):
    try:
        i = int(s)
        factor = "s"
    except ValueError:
        i = int(s[:-1])
        factor = s[-1]
        
    match factor:
        case "s":
            i *= 1
        case "m":
            i *= 60
        case "h":
            i *= 3600
        case "d":
            i *= 24 * 3600
        case "_":
            raise ValueError(f"Error parsing interval {s}")

    return timedelta(seconds=i)

def parse_plot_cfg(plot_raw):
    plot_raw['yaxis'] = [cfg_yaxis_defaults | ax for ax in plot_raw['yaxis']]
    if 'show_values_in_graph' in plot_raw:
        plot_raw['show_values_in_graph'] = cfg_show_values_in_graph_defaults | plot_raw['show_values_in_graph']
    return cfg_plot_defaults | plot_raw

def parse_intervals(item_raw, interval_args):
    for k, v in item_raw.items():
        if k in interval_args:
            v = parse_interval(v)
        item_raw[k] = v

    return item_raw

def parse_item_cfg(item_raw):
    if item_raw['type'] == 'plot':
        itm = parse_plot_cfg(item_raw)
        itm = parse_intervals(itm, ['auto_refresh', 'show_last'])
        return itm
    if item_raw['type'] == 'current values':
        itm = parse_intervals(item_raw, ['auto_refresh'])
        return itm
    else:
        return item_raw


def cfg(path_cmdline=None):
    if path_cmdline is not None:
        path = path_cmdline
    elif 'TSBOARD_CONFIG_PATH' in os.environ:
        path = os.environ['TSBOARD_CONFIG_PATH']
    else:
        path = default_cfg_path

    if not os.path.isfile(path):
        return cfg_defaults

    with open(path, mode="r") as f:
        y = cfg_defaults | yaml.safe_load(f)

    parsed_pages = []
    for p in y['pages']:
        parsed_pages.append(p | {'items' : [parse_item_cfg(itm) for itm in p['items']]})

    y['pages'] = parsed_pages

    for b, v in y['storage']['binning'].items():
        y['storage']['binning'][b]['start'] = parse_interval(v['start']).seconds * 1000
        binning = LogBinning(**y['storage']['binning'][b])
        y['storage']['binning'][b] = binning

        if b == '_default':
            y['storage']['default_binning'] = binning

    if 'default_binning' in y['storage']:
        del y['storage']['binning']['_default']

    if not 'path' in y['storage']:
        raise ValueError("Path to database must be given")
    
    return y

if __name__ == '__main__':
    import pprint
    pprint.pprint(cfg('config.yml'))

