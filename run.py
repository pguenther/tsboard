import argparse
from tsboard.app import create_app

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('-c', '--config', required=False, default=None)
    parsed = parser.parse_args()

    app = create_app(parsed.config)
    app.run(debug=app.config['debug'], host=app.config['host'])
